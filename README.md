sudoku-solver
=============

A non-brute-force method of solving simple newspaper sudokus.

This was a project in my earlier years, to get a hang of object-oriented programming in Java. On my commute to work, I used to take the subway a lot, I would inconsistently get the free newspaper subway. On that, there was the Sudoku, and I would usually solve it in the span of my ride. However, I sometimes didn't have a way to check my answers, and so I made this program to calculate the solution to a sudoku.

The project is a pretty straightforward one: the main handler opens up a SudokuBoard class, which contains the board, and all the possibilities at each square, and calls certain logic solvers, which essentially does set theory manipulation for each row, column, and 3x3 box. The two main algorithms employed are the Naked Single (http://www.oubk.com/Techniques/Naked-Single-Technique.html) and the Hidden Single (http://www.oubk.com/Techniques/Hidden-Single-Technique.html). These two methods are the most primarily used ones (and, the only ones our human brains quickly!) without 

Improvements:
- Timing of algorithm. Right now, a newspaper sudoku can be solved within 0.1s. However, for harder boards, that will not be the case. Some simple output of timing and number of computations run can be helpful.
- Other algorithms for harder sudokus. Hidden doubles and some wing-type algorithms are in the mix. This would require an overhaul in the board class, as a lot of them are chains, and I might have to model the board as a linked list/graph in conjunction.
- Better output and UI. So far, input is essentially computer-like input of dots and numbers. Hoping to generate a board to enter numbers and solve, as opposed to relying on System.out.println() the whole damn time. 
