package com.ivanpeng.math.sudoku;

import java.io.IOException;

/**
 * This class describes the basic data structure of the sudoku board.
 * Templates include a 2d array, and later on linked list structure for chains 
 * Notes:
 * - have to keep in mind the counting-by-C and human counting; currently begins at 1 and 0 denotes unknown.
 * - algorithms go in order of naked single, hidden single, naked double, hidden double
 * - naked single: after elimination, find the ones with only size 1 & are 0 on the board
 * - hidden single: find unique number in a set (ie. row, column, or box), by set theory algorithm
 * 		or by going through each number and checking if there is only one instance in its 3 sets.
 * - naked double: go through all size 2 cells in a set, check for any identical. if there is, remove all instances
 * 		of both numbers in all other cells. need a method to remember which numbers were checked
 * - hidden double: go through all possibilites in a set and if a 2 possibility arrays have 2 numbers intersecting
 * 		then remove all other possibilities in those 2 possibility arrays.
 * - formula for temp to box conversion: A[row][col] = B[row/3*3+col/3][row%3*3+col%3]
 * 
 * @author ivan
 *
 */
public class SudokuMap {

	public static final int size = 9;
	private String mapInput;
	protected int[][] map; // The default map board
	protected int[][] columnMap;
	protected int[][] boxMap;
	protected int[][] sizeMap; // The number of possibilities for each cell.
	 // The actual possibilities; true if still possible, false if no longer a possiblility
	protected boolean [][][] possibilities;
	
	
	public SudokuMap ()	{
	} // sudoku
	
	public void init()	{
		map = new int [size][size];
		sizeMap = new int [size][size];
		possibilities = new boolean[size][size][size];
		// set all possibilities to false first to initialize.
		for (int i = 0; i < size; i++)	{
			for (int j = 0; j < size; j++)
				for (int k = 0; k < size; k++)
					possibilities[i][j][k] = false; // SET TO TRUE?
		}
		// now set the map from mapString
		for (int i = 0; i < size; i++)	{
			for (int j = 0; j < size; j++)	{
				try	{
					int number = Integer.parseInt(mapInput.substring(i*size+j, i*size+j+1));
					map[i][j] = number;
				} catch (NumberFormatException nfex)	{
					map[i][j] = 0;
				} catch (NullPointerException nex)	{
					// do nothing; just pass. should not exit like this though...
				}
			}
		}
	}
	
	public void print()	{
		for (int i = 0; i < size; i++)	{
			if (i != 0 && i%3 == 0)
				System.out.print("\n");
			for (int j = 0; j < size; j++)	{
				System.out.print(map[i][j]);
				if (j%3 == 2)
					System.out.print(" ");
			}
		}
	}
	
	public void setMapInput (String mapInput)	{
		this.mapInput = mapInput;
	}
}
