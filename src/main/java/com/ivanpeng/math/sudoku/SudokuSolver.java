package com.ivanpeng.math.sudoku;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * This main class for the Sudoku Solver program. This takes a map injected 
 * from Spring and solves it with the libraries in the class LogicSolver. 
 * @author ivanpeng
 *
 */
public class SudokuSolver 
{
    public static void main( String[] args )
    {
    	BeanFactory factory = new ClassPathXmlApplicationContext("SudokuSolver.xml");
    	SudokuMap sudokuMap = (SudokuMap) factory.getBean("sudokuEasy");
    	sudokuMap.init();
    	LogicSolver logicSolver = new LogicSolver(sudokuMap);
    	//logicSolver.init();
    	logicSolver.solve();
    	sudokuMap = logicSolver.getSudokuMap();
    	System.out.println("\n\n");
        System.out.println( "Hello World!" );
    }
}
