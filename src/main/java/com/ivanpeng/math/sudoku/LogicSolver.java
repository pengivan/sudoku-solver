package com.ivanpeng.math.sudoku;

import java.util.ArrayList;
import java.util.List;

public class LogicSolver extends SudokuMap {

	private SudokuMap sudokuMap;
	
	public LogicSolver () {
	}
	
	public LogicSolver(SudokuMap sudokuMap)	{
		this.sudokuMap = sudokuMap;
	}
	
	/*
	public void init ()	{
		// set boxes and column maps.
	}
	*/
	/*
	 * Main solving function. Abstracted high up to call solve on different methods and check the
	 * difference between the current and previous board.
	 */
	public void solve()	{
	}
	
	public void eliminate ()	{
		// This is called when a box is determined to be valid.
	}
	
	public void removeRow (int row, int col, int key)	{
		for (int i = 0; i < col; i++)	{
			if (sudokuMap.sizeMap[row][i] != 1 && sudokuMap.possibilities[row][i][key-1] != false)	{
				sudokuMap.possibilities[row][i][key-1] = false;
				sudokuMap.sizeMap[row][i]--;
			}
		}
		// skip the element of concern; iterate through to the end after.
		for (int j = col+1; j < sudokuMap.size; j++)	{
			if (sudokuMap.sizeMap[row][j] != 1 && sudokuMap.possibilities[row][j][key-1] != false)	{
				sudokuMap.possibilities[row][j][key-1] = false;
				sudokuMap.sizeMap[row][j]--;
			}
		}
	}
	
	public void removeCol (int row, int col, int key)	{
		for (int i = 0; i < row; i++)
			if (sudokuMap.sizeMap[i][col] != 1 && sudokuMap.possibilities[i][col][key-1] != false)	{
				sudokuMap.possibilities[i][col][key-1] = false;
				sudokuMap.sizeMap[i][col]--;
			}
		for (int j = row+1; j < sudokuMap.size; j++)	
			if (sudokuMap.sizeMap[j][col] != 1 && sudokuMap.possibilities[j][col][key-1] != false)	{
				sudokuMap.possibilities[j][col][key-1] = false;
				sudokuMap.sizeMap[j][col]--;
			}
	}
	
	/* 
	 * This method removes key element in the box. However, a transformation must be done to 
	 * properly iterate through each box.
	 * The formula for transformation is:
	 * A[row][col] = B[row/3*3+col/3][row%3*3+col%3]
	 */
	public void removeBox (int row, int col, int key)	{
		int temp = row%3*3+col/3*3;
		int Irow = row/3*3;
		int Icol = col/3*3;
		for (int i = 0; i < temp; i++)	{
			if (sudokuMap.sizeMap[Irow+i/3][Icol+i%3] != 1 && sudokuMap.possibilities[Irow+i/3][Icol+i%3][key-1] != false)	{
				sudokuMap.possibilities[Irow+i/3][Icol+i%3][key-1] = false;
				sudokuMap.sizeMap[Irow+i/3][Icol+i%3]--;
			}
		}
		for (int j = temp+1; j < sudokuMap.size; j++)	{

			if (sudokuMap.sizeMap[Irow+j/3][Icol+j%3] != 1 && sudokuMap.possibilities[Irow+j/3][Icol+j%3][key-1] != false)	{
				sudokuMap.possibilities[Irow+j/3][Icol+j%3][key-1] = false;
				sudokuMap.sizeMap[Irow+j/3][Icol+j%3]--;
			}
		}
	}
	
	/*
	 * This function emulates the naked single method of solving. If there is one option left in the
	 * cell, then that is written down.
	 */
	public void fullHouse ()	{
		for (int i = 0; i < size; i++)	{
			for (int j = 0; j < size; j++)	{
				if (sudokuMap.map[i][j] == 0 && sizeMap[i][j] == 1)	{
					sudokuMap.map[i][j] = find(sudokuMap.possibilities[i][j]);
					removeBox(i, j, sudokuMap.map[i][j]);
					removeRow(i, j, sudokuMap.map[i][j]);
					removeCol(i, j, sudokuMap.map[i][j]);
				}
			}
		}
	}
	
	public int find (boolean[] binaryArray)	{
		int key=-1;
		for (int i = 0; i < size; i++)	{
			if (binaryArray[i] == true)	{
				key = i;
				break;
			}
		}
		return key+1;
	}
	
	/*
	 * Hidden single algorithm. Most fundamental in solving sudoku puzzles.
	 */
	public void hiddenSingle ()	{
		boolean[][] cols = new boolean[size][size];
		boolean[][] boxes = new boolean[size][size];
		for (int i = 0; i < size; i++)	{
			findUnique(sudokuMap.possibilities[i], sudokuMap.map, i, 1);
			for (int j = 0; j < size; j++)
				for (int k = 0; k < size; k++)
					cols[j][k] = sudokuMap.possibilities[j][i][k];
			findUnique(cols, sudokuMap.columnMap, i, 2);
			for (int j = 0; j < size; j++)
				for (int k = 0; k < size; k++)	
					boxes[j][k] = sudokuMap.possibilities[(i/3)*3+(j/3)][(i%3)*3+j%3][k];
			findUnique(boxes, sudokuMap.boxMap, i, 3);
		}
	}
	
	/*
	 * This function analyzes the current set and finds any unique numbers
	 * in the possibility array. If found, it will change and call the remove
	 * function.
	 * The mapToSet parameter describes the transformed puzzle. The identifier describes
	 * what the mapToSet variable has been transformed to (row, column, or box)
	 */
	public void findUnique(boolean[][]set, int[][] mapToSet, int index, int identifier)	{
		List<Integer> key = new ArrayList<Integer>();
		for (int i = 0; i < size; i++)	{
			for (int j = 0; j < size; j++)	{
				if (set[j][i] == true && mapToSet[index][j] == 0)	{
					key.add(j);
				}
			}
			if (key.size() == 1)	{
				if (identifier == 1)	{
					sudokuMap.map[index][key.get(0)] = i+1;
					this.removeRow(index, key.get(0), i+1);
					this.removeCol(index, key.get(0), i+1);
					this.removeBox(index, key.get(0), i+1);
				}
				else if (identifier == 2)	{
					sudokuMap.map[key.get(0)][index] = i+1;
					this.removeRow(key.get(0), index, i+1);
					this.removeCol(key.get(0), index, i+1);
					this.removeBox(key.get(0), index, i+1);
				}
				else if (identifier == 3)	{
					sudokuMap.map[index/3*3+key.get(0)/3][index%3*3+key.get(0)%3] = i+1;
					this.removeRow(index/3*3+key.get(0)/3, index%3*3+key.get(0)%3, i+1);
					this.removeCol(index/3*3+key.get(0)/3, index%3*3+key.get(0)%3, i+1);
					this.removeBox(index/3*3+key.get(0)/3, index%3*3+key.get(0)%3, i+1);
				}
			}
		}
	}

	public SudokuMap getSudokuMap() {
		return sudokuMap;
	}

	public void setSudokuMap(SudokuMap sudokuMap) {
		this.sudokuMap = sudokuMap;
	}
	
	
}
